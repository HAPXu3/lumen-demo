<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('users', 'UserController@list');
    $router->post('users', 'UserController@create');
    $router->put('users/{id}', 'UserController@update');
    $router->delete('users/{id}', 'UserController@delete');

    $router->get('user_events', 'UserEventController@list');
});
