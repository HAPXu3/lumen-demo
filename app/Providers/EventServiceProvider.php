<?php

namespace App\Providers;

use App\Events\User\{UserCreated, UserDeleted, UserUpdated};
use App\Listeners\User\{RecordUserCreation, RecordUserDeletion, RecordUserUpdate};
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        UserCreated::class => [
            RecordUserCreation::class,
        ],
        UserUpdated::class => [
            RecordUserUpdate::class,
        ],
        UserDeleted::class => [
            RecordUserDeletion::class,
        ],
    ];
}
