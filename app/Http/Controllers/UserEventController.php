<?php
declare(strict_types = 1);

namespace App\Http\Controllers;

use App\Domain\Services\UserService;

class UserEventController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function list()
    {
        return response()->json($this->userService->events());
    }
}
