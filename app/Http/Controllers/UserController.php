<?php
declare(strict_types = 1);

namespace App\Http\Controllers;

use App\Domain\Services\UserService;
use Illuminate\Validation\Rule;
use App\Events\User\{UserCreated, UserDeleted, UserUpdated};
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function list()
    {
        return response()->json($this->userService->findAll());
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'password' => 'required|min:6|max:255',
            'email' => 'required|email|unique:users|max:255',
        ]);

        $user = $this->userService->create(
            $request->input('name'),
            $request->input('password'),
            $request->input('email'),
        );

        event(new UserCreated($user));

        return response()->json($user, 201);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'max:255',
            'password' => 'min:6|max:255',
            'email' => ['email', Rule::unique('users')->ignore($id), 'max:255'],
        ]);

        $user = $this->userService->update($id, $request->input());

        event(new UserUpdated($user, $user->getChanges()));

        return response()->json($user, 200);
    }

    public function delete($id)
    {
        $user = $this->userService->delete($id);

        event(new UserDeleted($user));

        return response('Deleted', 204);
    }
}
