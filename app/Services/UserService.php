<?php
declare(strict_types = 1);

namespace App\Services;

use App\Domain\Services\UserService as UserServiceInterface;
use App\Models\User;
use App\Models\UserEvent;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserService implements UserServiceInterface
{
    public function findAll()
    {
        return User::all();
    }

    public function create(string $name, string $password, string $email): User
    {
        $user = new User([
            'name' => $name,
            'password' => Hash::make($password),
            'email' => $email,
        ]);

        $user->setAttribute('api_token', Str::random());

        $user->saveOrFail();
        return $user;
    }

    public function update($id, array $requestData): User
    {
        $user = User::findOrFail($id);
        $user->fill($requestData);

        if ($user->isDirty('password')) {
            $user->setAttribute('password', Hash::make($user->getAttribute('password')));
        }

        $user->saveOrFail();
        return $user;
    }

    public function delete($id): User
    {
        $user = User::findOrFail($id);
        $user->delete();
        return $user;
    }

    public function events()
    {
        return UserEvent::all();
    }
}
