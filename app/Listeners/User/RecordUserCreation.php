<?php
declare(strict_types = 1);

namespace App\Listeners\User;

use App\Events\User\UserCreated;
use App\Models\UserEvent;

class RecordUserCreation
{
    public function handle(UserCreated $event)
    {
        (new UserEvent([
            'type' => UserEvent::TYPE_CREATION,
            'data' => ['id' => $event->user->id],
        ]))->save();
    }
}
