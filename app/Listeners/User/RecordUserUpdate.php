<?php
declare(strict_types = 1);

namespace App\Listeners\User;

use App\Events\User\UserUpdated;
use App\Models\UserEvent;

class RecordUserUpdate
{
    public function handle(UserUpdated $event)
    {
        (new UserEvent([
            'type' => UserEvent::TYPE_UPDATE,
            'data' => [
                'id' => $event->user->id,
                'fields' => $event->fields,
            ],
        ]))->save();
    }
}
