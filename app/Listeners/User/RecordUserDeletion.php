<?php
declare(strict_types = 1);

namespace App\Listeners\User;

use App\Events\User\UserDeleted;
use App\Models\UserEvent;

class RecordUserDeletion
{
    public function handle(UserDeleted $event)
    {
        (new UserEvent([
            'type' => UserEvent::TYPE_DELETION,
            'data' => $event->user->attributesToArray(),
        ]))->save();
    }
}
