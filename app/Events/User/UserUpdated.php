<?php
declare(strict_types = 1);

namespace App\Events\User;

use App\Models\User;

class UserUpdated extends UserEvent
{
    public $fields = [];

    public function __construct(User $user, array $fields)
    {
        parent::__construct($user);

        $this->fields = $fields;
    }
}
