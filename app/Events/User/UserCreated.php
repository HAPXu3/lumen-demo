<?php
declare(strict_types = 1);

namespace App\Events\User;

class UserCreated extends UserEvent
{
}
