<?php
declare(strict_types = 1);

namespace App\Domain\Services;

use App\Models\User;

interface UserService
{
    public function findAll();

    public function create(string $name, string $password, string $email): User;

    public function update($id, array $requestData): User;

    public function delete($id): User;

    public function events();
}
