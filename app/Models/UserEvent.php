<?php
declare(strict_types = 1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserEvent extends Model
{
    public const CREATED_AT = 'time';

    public const UPDATED_AT = null;

    public const TYPE_CREATION = 1;

    public const TYPE_UPDATE = 2;

    public const TYPE_DELETION = 3;

    protected $casts = [
        'data' => 'array',
    ];

    protected $fillable = [
        'type', 'data',
    ];
}
